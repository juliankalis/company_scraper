import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

branches = list()
client_email = list()
client_phone = list()
client_contact = list()

# --- get a list of all existing branches

def get_branches():

    for page in range(1,10):

        url = 'https://www.kmu-aargau.ch/?page=' + str(page)
        r = requests.get(url).text
        soup = BeautifulSoup(r, "lxml")

        div = soup.find("div", {"class": "branchen"})
        li_elements = div.find_all("li")

        for item in li_elements:
            
            link = item.find("a", href=True)
            link = link["href"]
            link = str(link).replace(".","")
            branches.append(link)
            
            #print(link)
        #print(len(branchen))


# --- for every branch, navigate to page and get companies

def get_companies():

    for page in branches:

        url = 'https://www.kmu-aargau.ch/' + str(page)
        r = requests.get(url).text
        soup = BeautifulSoup(r, "lxml")

        div = soup.find("div", {"class", "adresse details"})
        a_element = div.find("a", href=True)["href"]
        get_company_information(a_element)
# --- for every company, get contact information (function)

def get_company_information(a_element):
    
    url = 'https://www.kmu-aargau.ch/' + str(a_element)
    r = requests.get(url).text
    soup = BeautifulSoup(r, "lxml")

    div = soup.findAll("div", attrs={"class", "details"})

    for i in div:

        #print(i.find("p").text)
        p = i.findAll("p")

        for item in p:

            if str(item.text).__contains__("Kontaktperson:"):

                contact_person = str(item.text).split(":")[1].strip()
                print(contact_person)
            else:
                pass

            if str(item.text).__contains__("Telefon:"):

                phone_number = str(item.text).split("Fax")[0].split(":")[1].strip()
                print(phone_number)
            else:
                pass
            
            a_list = item.findAll("a")

            for item in a_list:

                if str(item["href"]).__contains__("mailto"):

                    mail = str(item["href"]).split(":")[1]
                    print(mail)
                else:
                    pass
        
        try:
            client_contact.append(contact_person)
        except UnboundLocalError:
            client_contact.append("N/A")
        try:
            client_email.append(mail)
        except UnboundLocalError:
            client_email.append("N/A")
        try:
            client_phone.append(phone_number)            
        except UnboundLocalError:
            client_phone.append("N/A")

# --- call funtions

get_branches()
get_companies()

print(len(client_email))
print(len(client_phone))

potential_clients = pd.DataFrame(list(zip(client_email, client_phone, client_contact)), columns=["email", "phone", "contact"])

print(potential_clients)

potential_clients.to_csv("path/to/save/csv", index=None)